@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-default">Go Back</a>
    <h1>{{$post->title}}</h1>
    <img style="width:80%" src="/storage/cover_images/{{$post->cover_image}}">
    <br><br>
    <small>Written on {{$post->created_at}}  by {{$post->user->name}}</small>
    <div>
        {!!$post->body!!}
    </div>
    <hr>
    @if(!Auth::guest())
        @if(Auth::user()->id == $post->user_id)
        <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a>

    

        {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST','onsubmit' => 'return confirmDelete()'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
        {!!Form::close()!!}   
        @endif
    @endif
@endsection

    <script>
    function confirmDelete() {
    var result = confirm('Are you sure you want to delete?');

    if (result) {
            return true;
        } else {
            return false;
        }
    }   
    </script>
        
    