@extends('layouts.app')

@section('content')
      <div class="jumbotron text-center">
            <h1>Welcome To LSAPP</h1>
            <p>This is a laravel Application</p>
            @if(Auth::guest())
                  <p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a> 
                     <a class="btn btn-success btn-lg" href="/register" role="button">Register</a>
                  </p>

            
            @else
            <p>Welcome {{ Auth::user()->name }}!!!</p>
            @endif
      </div>
@endsection 